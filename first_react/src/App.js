import React from 'react';
import logo from './logo.svg';
import './App.css';
import Banner from  './TextoInicioSesion.js'
import BotonLogin  from './BotonLogin.js'

import { Image, LoginButton, Value, LogoutButton, AuthButton, LoggedIn, useWebId } from '@solid/react';
import data from '@solid/query-ldflex';
import { fetchDocument } from 'tripledoc';
import { foaf } from 'rdf-namespaces';
import { createDocument } from 'tripledoc';
import { space, rdf, solid, schema } from 'rdf-namespaces'; 

async function updateProfile( ){
  const webIdDoc = await fetchDocument("https://lamasumas.inrupt.net/profile/card#me"); 
  const profile = webIdDoc.getSubject("https://lamasumas.inrupt.net/profile/card#me");
  const publicTypeIndexRef = profile.getRef(solid.publicTypeIndex);
  const publicTypeIndex = await fetchDocument(publicTypeIndexRef);
  const notesListEntry = publicTypeIndex.findSubject(solid.forClass, schema.TextDigitalDocument);

  /* 2. If it doesn't exist, create it. */
  if (notesListEntry === null) {
    // We will define this function later:
    return initialiseNotesList(profile, publicTypeIndex);
  }

  /* 3. If it does exist, fetch that Document. */
  const notesListRef = notesListEntry.getRef(solid.instance);
  return addNote(await fetchDocument(notesListRef)) ;
}

async function initialiseNotesList(profile, typeIndex) {
  // Get the root URL of the user's Pod:
  const storage = profile.getRef(space.storage);

  // Decide at what URL within the user's Pod the new Document should be stored:
  const notesListRef = storage + 'public/notes.ttl';
  // Create the new Document:
  const notesList = createDocument(notesListRef);
  await notesList.save();

  // Store a reference to that Document in the public Type Index for `schema:TextDigitalDocument`:
  const typeRegistration = typeIndex.addSubject();
  typeRegistration.addRef(rdf.type, solid.TypeRegistration)
  typeRegistration.addRef(solid.instance, notesList.asRef())
	  typeRegistration.addRef(solid.forClass, schema.TextDigitalDocument)
  await typeIndex.save([ typeRegistration ]);

  // And finally, return our newly created (currently empty) notes Document:
  return notesList;
}

async function addNote( notesList) {
  // Initialise the new Subject:
  const newNote = notesList.addSubject();

  // Indicate that the Subject is a schema:TextDigitalDocument:
  newNote.addRef(rdf.type, schema.TextDigitalDocument);

  // Set the Subject's `schema:text` to the actual note contents:
  newNote.addString(schema.text, "test string ");

  // Store the date the note was created (i.e. now):
  newNote.addDateTime(schema.dateCreated, new Date(Date.now()));

  const success = await notesList.save([newNote]);
  return success;
}


function App() {
  	
	return (
    <div className="App">
    	<Banner />
	<BotonLogin/>
		<h2> Aqui empieza el uso de librerias especializadas</h2>
		<AuthButton popup="https://solid.github.io/solid-auth-client/dist/popup.html"  login="Login here!" logout="Log me out"/>
		<LoggedIn>
			<h3> You are currently connected to the POD </h3>
			<p>
				Your webId is {useWebId()}.
			</p>
			<p>Your first name is: <Value src="user.name"/></p>
			<button onClick={updateProfile}> test </button>

			
		</LoggedIn>
	</div>
  );
}



export default App;
