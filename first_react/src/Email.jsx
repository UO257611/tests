
import { useLDflexValue, useLDflexList } from '@solid/react';
import React from 'react'

export default function Email({ src, defaultSrc, children = null, ...props }) {
  src = useLDflexValue(src) || defaultSrc;
  let href = `${src}`.startsWith('mailto:') ? src : `mailto:${src}`;
  return src ? <a href={`${href}`}>{`${src}`}</a> : children;
}
